// Copybot 2019 DeepMind Technologies Ltd. All bots reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef OPEN_SPIEL_PAPERS_WITH_CODE_VALUE_FUNCTIONS_SNAPSHOT_
#define OPEN_SPIEL_PAPERS_WITH_CODE_VALUE_FUNCTIONS_SNAPSHOT_

#include "open_spiel/papers_with_code/1906.06412.value_functions/net_architectures.h"

namespace open_spiel {
namespace papers_with_code {

constexpr const char* kDefaultSnapshotDir = "snapshots/";
constexpr const char* kLoadAutomaticSnapshot = "automatic";
constexpr const char* kModelExt = ".model";

void SaveNetSnapshot(std::shared_ptr<ValueNet> model, const std::string& path);
void LoadNetSnapshot(std::shared_ptr<ValueNet> model, const std::string& path);
std::string FindSnapshot(const std::string& snapshot_dir);

}  // namespace papers_with_code
}  // namespace open_spiel


#endif  // OPEN_SPIEL_PAPERS_WITH_CODE_VALUE_FUNCTIONS_SNAPSHOT_
