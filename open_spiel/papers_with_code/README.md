# Papers with code

This directory contains code for experiments done in papers released on 
[arXiv.org](https://arxiv.org/). Source code in this directory is not built 
by default and a config option has to be turned on. To enable the compilation,  
set `BUILD_WITH_PAPERS=ON` (see file `global_variables.sh` for details).

If you'd like to add a new paper, please use the [template](TEMPLATE.md) 
and issue a pull request to this repository. Feel free to add your paper also to  
the list of [papers with code](https://paperswithcode.com/).

Minimize number of dependencies!
