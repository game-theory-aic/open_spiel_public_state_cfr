// Copyright 2019 DeepMind Technologies Ltd. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "open_spiel/algorithms/bandits.h"

#include "open_spiel/spiel.h"
#include "open_spiel/spiel_utils.h"

namespace open_spiel {
namespace algorithms {
namespace bandits {
namespace {

void TestRegretMatching() {
  // TODO.
}
void TestRegretMatchingPlus() {
  // TODO.
}
void TestPredictiveRegretMatching() {
  // TODO.
}
void TestPredictiveRegretMatchingPlus() {
  // TODO.
}
void TestFollowTheLeader() {
  // TODO.
}
void TestFollowTheRegularizedLeader() {
  // TODO.
}
void TestPredictiveFollowTheRegularizedLeader() {
  // TODO.
}
void TestOptimisticMirrorDescent() {
  // TODO.
}
void TestPredictiveOptimisticMirrorDescent() {
  // TODO.
}
void TestExp3() {
  // TODO.
}
void TestExp4() {
  // TODO.
}
void TestDiscountedRegretMatching() {
  // TODO.
}
void TestHedge() {
  // TODO.
}
void TestOptimisticHedge() {
  // TODO.
}
void TestUpperConfidenceBounds() {
  // TODO.
}
void TestEpsGreedy() {
  // TODO.
}

} // namespace
}  // namespace bandits
}  // namespace algorithms
}  // namespace open_spiel

int main(int argc, char** argv) {
  open_spiel::algorithms::bandits::TestRegretMatching();
  open_spiel::algorithms::bandits::TestRegretMatchingPlus();
  open_spiel::algorithms::bandits::TestPredictiveRegretMatching();
  open_spiel::algorithms::bandits::TestPredictiveRegretMatchingPlus();
  open_spiel::algorithms::bandits::TestFollowTheLeader();
  open_spiel::algorithms::bandits::TestFollowTheRegularizedLeader();
  open_spiel::algorithms::bandits::TestPredictiveFollowTheRegularizedLeader();
  open_spiel::algorithms::bandits::TestOptimisticMirrorDescent();
  open_spiel::algorithms::bandits::TestPredictiveOptimisticMirrorDescent();
  open_spiel::algorithms::bandits::TestExp3();
  open_spiel::algorithms::bandits::TestExp4();
  open_spiel::algorithms::bandits::TestDiscountedRegretMatching();
  open_spiel::algorithms::bandits::TestHedge();
  open_spiel::algorithms::bandits::TestOptimisticHedge();
  open_spiel::algorithms::bandits::TestUpperConfidenceBounds();
  open_spiel::algorithms::bandits::TestEpsGreedy();
}
