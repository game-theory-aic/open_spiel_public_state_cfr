// Copyright 2019 DeepMind Technologies Ltd. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef OPEN_SPIEL_ALGORITHMS_BANDITS_H_
#define OPEN_SPIEL_ALGORITHMS_BANDITS_H_

#include <algorithm>
#include <memory>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include "open_spiel/spiel.h"
#include "open_spiel/spiel_utils.h"
#include "open_spiel/policy.h"

// This file contains implementations of (multi-armed) bandit algorithms.
//
// At each time `t`, a bandit with `n` arms for the online linear optimization
// (OLO) problem supports the following two operations:
//
// 1. ComputeStrategy() computes a strategy `x_t ∈ S^n` (probability simplex
//    `S^n ⊆ R^n`).
// 2. ObserveRewards() receives a reward vector `r_t` that is meant to evaluate
//    the strategy `x_t` that was last computed.
//
// Specifically, the bandit receives an expected reward equal to inner product
// of `r_t` and `x_t`. The reward vector `r_t` can depend on all past strategies
// that were // output by the bandit. The bandit operates online in the sense
// that each strategy `x_t` can depend only on the decision `x_1 , ... ,
// x_(t−1)` output in the past, as well as the reward vectors `r_1 , ... ,
// r_(t−1)` that were observed in the past. No information about the future
// rewards `r_t, r_(t+1), ...` is available to the bandit at time `t`.
//
// An example of a computation of a reward function: if bandit algorithms are
// used on a zero-sum normal-form game described by a payoff matrix `A` for the
// first player, then the reward vector `r_t` for the first player is `Ay` and
// `-x^T A` for the second player, where `x` and `y` are the strategies for the
// players one and two respectively.

namespace open_spiel {
namespace algorithms {
namespace bandits {

// Base class for all bandit algorithms.
class Bandit {
 protected:
  std::vector<double> current_strategy_;
 public:
  Bandit(int num_actions) :
      current_strategy_(num_actions, 1. / num_actions) {
    SPIEL_CHECK_GT(num_actions, 0);
  }
  Bandit(std::vector<double> initial_strategy) :
      current_strategy_(std::move(initial_strategy)) {
    SPIEL_CHECK_FALSE(current_strategy_.empty());
  }
  virtual ~Bandit() = default;
  // Return the positive number of actions (arms) available to the bandit.
  int num_actions() const { return current_strategy_.size(); }

  // Reset the bandit to the same state as when it was constructed.
  virtual void Reset() {
    std::fill(current_strategy_.begin(), current_strategy_.end(),
              1. / num_actions());
  }

  // Compute the strategy `x_t` and save it into current_strategy().
  //
  // Optionally, the algorithm receives a weight it should put on the strategy.
  // This is intended for the use case within counter-factual regret
  // minimization framework, and the weight is the reach probability of the
  // current strategy.
  virtual void ComputeStrategy(size_t current_time, double weight = 1.) = 0;

  // Return the strategy `x_t`
  const std::vector<double>& current_strategy() const {
    return current_strategy_;
  }

  // Observe the rewards `r_t` incurred after the strategy `x_t` was used.
  virtual void ObserveRewards(absl::Span<const double> rewards) = 0;

  // Does this bandit compute also an average strategy?
  virtual bool uses_average_strategy() const { return false; }
  virtual std::vector<double> AverageStrategy() const {
    SpielFatalError("AverageStrategy() is not implemented.");
  }

  // Does this bandit use (externally supplied) predictions?
  // If it does, the function `ObservePrediction()` is called before each call
  // of `ComputeStrategy()`.
  virtual bool uses_predictions() const { return false; }
  virtual void ObservePrediction(absl::Span<const double> prediction) {
    SpielFatalError("ObservePrediction() is not implemented.");
  }

  // Does this bandit use a context for computation of its strategy?
  // If it does, the function `ObserveContext()` is called before each call
  // of `ComputeStrategy()` and `ObservePrediction()`.
  virtual bool uses_context() const { return false; }
  virtual void ObserveContext(absl::Span<const double> context) {
    SpielFatalError("ObserveContext() is not implemented.");
  }
};

// -----------------------------------------------------------------------------
// Specific bandits.
// -----------------------------------------------------------------------------

// A bandit that always a uniform strategy.
class UniformStrategy final : public Bandit {
 public:
  UniformStrategy(int num_actions) : Bandit(num_actions) {}
  void ComputeStrategy(size_t current_time, double weight = 1.) override {}
  void ObserveRewards(absl::Span<const double> rewards) override {}
  void Reset() override {}  // No need to reset anything.
};

// A bandit that always returns the same strategy.
class FixedStrategy final : public Bandit {
 public:
  FixedStrategy(const std::vector<double>& fixed_strategy)
      : Bandit(fixed_strategy.size()) {
    SPIEL_CHECK_TRUE(IsValidProbDistribution(fixed_strategy));
    std::copy(fixed_strategy.begin(), fixed_strategy.end(),
              current_strategy_.begin());
  }
  void ComputeStrategy(size_t current_time, double weight = 1.) override {}
  void ObserveRewards(absl::Span<const double> rewards) override {}
  void Reset() override {}  // No need to reset anything.
  std::vector<double> AverageStrategy() const override{
    return current_strategy();
  };
};

// A bandit that can be dynamically set to return a requested strategy,
// via mutable_strategy(). Reset() makes it to be uniform.
class FixableStrategy : public Bandit {
 public:
  FixableStrategy(int num_actions) : Bandit(num_actions) {}
  FixableStrategy(std::vector<double> initial_strategy)
    : Bandit(std::move(initial_strategy)) {}
  // Return a writable view for the strategy.
  absl::Span<double> mutable_strategy() {
    return absl::MakeSpan(current_strategy_);
  }
  void ComputeStrategy(size_t current_time, double weight = 1.) override {}
  void ObserveRewards(absl::Span<const double> rewards) override {}
  std::vector<double> AverageStrategy() const override{
      return current_strategy();
  };
};

// A bandit that chooses an action with the highest reward.
// In time t=1, it chooses uniform strategy.
class BestResponse final : public Bandit {
  size_t response_index_ = 0;
 public:
  BestResponse(int num_actions) : Bandit(num_actions) {}
  void ComputeStrategy(size_t current_time, double weight = 1.) override {
    if (current_time == 1) return;
    std::fill(current_strategy_.begin(), current_strategy_.end(), 0.);
    current_strategy_[response_index_] = 1.;
  }
  void ObserveRewards(absl::Span<const double> rewards) override {
    SPIEL_DCHECK_EQ(rewards.size(), current_strategy_.size());
    auto iter_max = std::max_element(rewards.begin(), rewards.end());
    response_index_ = std::distance(rewards.begin(), iter_max);
  };
  void Reset() override {
    Bandit::Reset();
    response_index_ = 0;
  }
};

// [1] A Simple Adaptive Procedure Leading to Correlated Equilibrium
//     Sergiu Hart, Andreu Mas‐Colell
//     http://wwwf.imperial.ac.uk/~dturaev/Hart0.pdf
class RegretMatching final : public Bandit {
  std::vector<double> cumulative_regrets_;
  std::vector<double> cumulative_strategy_;
 public:
  RegretMatching(int num_actions);
  bool uses_average_strategy() const override { return true; }

  void ComputeStrategy(size_t current_time, double weight = 1.) override;
  void ObserveRewards(absl::Span<const double> rewards) override;
  std::vector<double> AverageStrategy() const override;
  void Reset() override;
};

// [2] Solving Large Imperfect Information Games Using CFR+
//     Oskari Tammelin
//     https://arxiv.org/abs/1407.5042
class RegretMatchingPlus final : public Bandit {
  std::vector<double> cumulative_regrets_;
  std::vector<double> cumulative_strategy_;
 public:
  RegretMatchingPlus(int num_actions);
  bool uses_average_strategy() const override { return true; }

  void ComputeStrategy(size_t current_time, double weight = 1.) override;
  void ObserveRewards(absl::Span<const double> rewards) override;
  std::vector<double> AverageStrategy() const override;
  void Reset() override;
  void RandomizeRegrets(std::mt19937& rnd, double min, double max);
};

// TODO(sustr): RM+ with epsilon convex combination with uniform strategy.
class RMPlusWithEps final : public Bandit {
  std::vector<double> cumulative_regrets_;
  std::vector<double> cumulative_strategy_;
 public:
  RMPlusWithEps(int num_actions);
  bool uses_average_strategy() const override { return true; }

  void ComputeStrategy(size_t current_time, double weight = 1.) override;
  void ObserveRewards(absl::Span<const double> rewards) override;
  std::vector<double> AverageStrategy() const override;
  void Reset() override;
};

// TODO(sustr): come up with a better name.
class ConstrainedRMPlus final : public Bandit {
  std::vector<double> cumulative_regrets_;
  std::vector<double> cumulative_strategy_;
 public:
  ConstrainedRMPlus(int num_actions);
  bool uses_average_strategy() const override { return true; }

  void ComputeStrategy(size_t current_time, double weight = 1.) override;
  void ObserveRewards(absl::Span<const double> rewards) override;
  std::vector<double> AverageStrategy() const override;
  void Reset() override;
};

// Faster Game Solving via Predictive Blackwell Approachability:
// Connecting Regret Matching and Mirror Descent
// Gabriele Farina, Christian Kroer, Tuomas Sandholm
// https://arxiv.org/abs/2007.14358
class PredictiveRegretMatching final : public Bandit {
  std::vector<double> cumulative_regrets_;
  std::vector<double> cumulative_strategy_;
  std::vector<double> prediction_;
 public:
  PredictiveRegretMatching(int num_actions);
  void ComputeStrategy(size_t current_time, double weight = 1.) override;
  void ObserveRewards(absl::Span<const double> rewards) override;

  bool uses_predictions() const override { return true; }
  void ObservePrediction(absl::Span<const double> rewards) override;

  bool uses_average_strategy() const override { return true; }
  std::vector<double> AverageStrategy() const override;
  void Reset() override;
};

// Faster Game Solving via Predictive Blackwell Approachability:
// Connecting Regret Matching and Mirror Descent
// Gabriele Farina, Christian Kroer, Tuomas Sandholm
// https://arxiv.org/abs/2007.14358
class PredictiveRegretMatchingPlus final : public Bandit {
  std::vector<double> cumulative_regrets_;
  std::vector<double> cumulative_strategy_;
  std::vector<double> prediction_;
 public:
  PredictiveRegretMatchingPlus(int num_actions);
  void ComputeStrategy(size_t current_time, double weight = 1.) override;
  void ObserveRewards(absl::Span<const double> rewards) override;

  bool uses_average_strategy() const override { return true; }
  std::vector<double> AverageStrategy() const override;

  bool uses_predictions() const override { return true; }
  void ObservePrediction(absl::Span<const double> rewards) override;

  void Reset() override;
};

// Follow-the-leader
// https://courses.cs.washington.edu/courses/cse599s/14sp/scribes/lecture2/scribeNote.pdf
// Isn't that just greedy??
class FollowTheLeader final : public Bandit {
 public:
  FollowTheLeader(int num_actions);
  bool uses_average_strategy() const override { return true; }
  bool uses_predictions() const override { return true; }

  void ComputeStrategy(size_t current_time, double weight = 1.) override;
  void ObserveRewards(absl::Span<const double> rewards) override;
  std::vector<double> AverageStrategy() const override;
  void Reset() override;
};

// Follow-the-regularized-leader
// http://www-stat.wharton.upenn.edu/~steele/Resources/Projects/SequenceProject/Hannan.pdf
// https://ttic.uchicago.edu/~shai/papers/ShalevSi07_mlj.pdf
// https://courses.cs.washington.edu/courses/cse599s/14sp/scribes/lecture3/lecture3.pdf
class FollowTheRegularizedLeader final : public Bandit {
 public:
  FollowTheRegularizedLeader(int num_actions,
                             std::function<double(
                                 std::vector<double>/*weight*/)> regularizer);
  bool uses_average_strategy() const override { return true; }
  bool uses_predictions() const override { return true; }

  void ComputeStrategy(size_t current_time, double weight = 1.) override;
  void ObserveRewards(absl::Span<const double> rewards) override;
  std::vector<double> AverageStrategy() const override;
  void Reset() override;
};

// Faster Game Solving via Predictive Blackwell Approachability:
// Connecting Regret Matching and Mirror Descent
// Gabriele Farina, Christian Kroer, Tuomas Sandholm
// https://arxiv.org/abs/2007.14358
class PredictiveFollowTheRegularizedLeader final : public Bandit {
 public:
  PredictiveFollowTheRegularizedLeader(int num_actions);
  bool uses_average_strategy() const override { return true; }
  bool uses_predictions() const override { return true; }

  void ComputeStrategy(size_t current_time, double weight = 1.) override;
  void ObserveRewards(absl::Span<const double> rewards) override;
  std::vector<double> AverageStrategy() const override;
  void Reset() override;
};

class OptimisticMirrorDescent final : public Bandit {
 public:
  OptimisticMirrorDescent(int num_actions);
  bool uses_average_strategy() const override { return true; }
  bool uses_predictions() const override { return true; }

  void ComputeStrategy(size_t current_time, double weight = 1.) override;
  void ObserveRewards(absl::Span<const double> rewards) override;
  std::vector<double> AverageStrategy() const override;
  void Reset() override;
};

class PredictiveOptimisticMirrorDescent final : public Bandit {
 public:
  PredictiveOptimisticMirrorDescent(int num_actions);
  bool uses_average_strategy() const override { return true; }
  bool uses_predictions() const override { return true; }

  void ComputeStrategy(size_t current_time, double weight = 1.) override;
  void ObserveRewards(absl::Span<const double> rewards) override;
  std::vector<double> AverageStrategy() const override;
  void Reset() override;
};

class Exp3 final : public Bandit {
 public:
  Exp3(int num_actions);
  bool uses_average_strategy() const override { return true; }
  bool uses_predictions() const override { return true; }

  void ComputeStrategy(size_t current_time, double weight = 1.) override;
  void ObserveRewards(absl::Span<const double> rewards) override;
  std::vector<double> AverageStrategy() const override;
  void Reset() override;
};

class Exp4 final : public Bandit {
 public:
  Exp4(int num_actions);
  bool uses_average_strategy() const override { return true; }
  bool uses_predictions() const override { return true; }

  void ComputeStrategy(size_t current_time, double weight = 1.) override;
  void ObserveRewards(absl::Span<const double> rewards) override;
  std::vector<double> AverageStrategy() const override;
  void Reset() override;
};

// Solving Imperfect-Information Gamesvia Discounted Regret Minimization
// Noam Brown, Tuomas Sandholm
// https://arxiv.org/pdf/1809.04040v3.pdf
class DiscountedRegretMatching final : public Bandit {
 public:
  DiscountedRegretMatching(int num_actions, double alpha, double beta,
                           double gamma);
  bool uses_average_strategy() const override { return true; }

  void ComputeStrategy(size_t current_time, double weight = 1.) override;
  void ObserveRewards(absl::Span<const double> rewards) override;
  std::vector<double> AverageStrategy() const override;
  void Reset() override;
};

class Hedge final : public Bandit {
 public:
  Hedge(int num_actions);
  bool uses_average_strategy() const override { return true; }

  void ComputeStrategy(size_t current_time, double weight = 1.) override;
  void ObserveRewards(absl::Span<const double> rewards) override;
  std::vector<double> AverageStrategy() const override;
  void Reset() override;
};

// https://arxiv.org/pdf/1507.00407.pdf
class OptimisticHedge final : public Bandit {
 public:
  OptimisticHedge(int num_actions);
  bool uses_average_strategy() const override { return true; }

  void ComputeStrategy(size_t current_time, double weight = 1.) override;
  void ObserveRewards(absl::Span<const double> rewards) override;
  std::vector<double> AverageStrategy() const override;
  void Reset() override;
};

class UpperConfidenceBounds final : public Bandit {
 public:
  UpperConfidenceBounds(int num_actions);
  bool uses_average_strategy() const override { return true; }
  bool uses_predictions() const override { return true; }

  void ComputeStrategy(size_t current_time, double weight = 1.) override;
  void ObserveRewards(absl::Span<const double> rewards) override;
  std::vector<double> AverageStrategy() const override;
  void Reset() override;
};

}  // namespace bandits

std::unique_ptr<bandits::Bandit> MakeBandit(
    const std::string& bandit_name, int num_actions,
    GameParameters bandit_params);

}  // namespace algorithms
}  // namespace open_spiel

#endif  // OPEN_SPIEL_ALGORITHMS_BANDITS_H_
