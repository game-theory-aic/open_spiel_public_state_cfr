game: turn_based_simultaneous_game(game=goofspiel(imp_info=True,num_cards=4,points_order=descending))

GameType.chance_mode = ChanceMode.EXPLICIT_STOCHASTIC
GameType.dynamics = Dynamics.SEQUENTIAL
GameType.information = Information.IMPERFECT_INFORMATION
GameType.long_name = "Turn-based Goofspiel"
GameType.max_num_players = 10
GameType.min_num_players = 2
GameType.parameter_specification = ["game"]
GameType.provides_information_state_string = True
GameType.provides_information_state_tensor = True
GameType.provides_observation_string = True
GameType.provides_observation_tensor = True
GameType.provides_factored_observation_string = True
GameType.reward_model = RewardModel.TERMINAL
GameType.short_name = "turn_based_simultaneous_game"
GameType.utility = Utility.ZERO_SUM

NumDistinctActions() = 4
PolicyTensorShape() = [4]
MaxChanceOutcomes() = 0
GetParameters() = {game=goofspiel(imp_info=True,num_cards=4,num_turns=-1,opponent_br_deck=False,players=2,points_order=descending,returns_type=win_loss)}
NumPlayers() = 2
MinUtility() = -1.0
MaxUtility() = 1.0
UtilitySum() = 0.0
InformationStateTensorShape() = turn_based_sim_played_action: [4], turn_based_sim_progress: [2], point_totals: [2, 11], player_hand: [4], win_sequence: [4, 2], tie_sequence: [4], point_card_sequence: [4, 4], player_action_sequence: [4, 4]
InformationStateTensorLayout() = TensorLayout.CHW
InformationStateTensorSize() = 76
ObservationTensorShape() = turn_based_sim_played_action: [4], turn_based_sim_progress: [2], current_point_card: [4], remaining_point_cards: [4], point_totals: [2, 11], player_hand: [4], win_sequence: [4, 2], tie_sequence: [4]
ObservationTensorLayout() = TensorLayout.CHW
ObservationTensorSize() = 52
MaxGameLength() = 8
ToString() = "turn_based_simultaneous_game(game=goofspiel(imp_info=True,num_cards=4,num_turns=-1,opponent_br_deck=False,players=2,points_order=descending,returns_type=win_loss))"

# State 0
# Partial joint action:
# P0 hand: 1 2 3 4  P1 hand: 1 2 3 4  P0 actions:  P1 actions:  Point card sequence: 4  Points: 0 0
IsTerminal() = False
History() = []
HistoryString() = ""
IsChanceNode() = False
IsSimultaneousNode() = False
CurrentPlayer() = 0
InformationStateString(0) = "Current player: 0\nP0 hand: 1 2 3 4  P0 action sequence:  Point card sequence: 4  Win sequence:  Points: 0 0  Terminal?: 0 "
InformationStateString(1) = "Current player: 0\nP1 hand: 1 2 3 4  P1 action sequence:  Point card sequence: 4  Win sequence:  Points: 0 0  Terminal?: 0 "
InformationStateTensor(0).turn_based_sim_played_action: ◯◯◯◯
InformationStateTensor(0).turn_based_sim_progress: ◉◯
InformationStateTensor(0).point_totals: ◉◯◯◯◯◯◯◯◯◯◯
                                        ◉◯◯◯◯◯◯◯◯◯◯
InformationStateTensor(0).player_hand: ◉◉◉◉
InformationStateTensor(0).win_sequence: ◯◯
                                        ◯◯
                                        ◯◯
                                        ◯◯
InformationStateTensor(0).tie_sequence: ◯◯◯◯
InformationStateTensor(0).point_card_sequence: ◯◯◯◉
                                               ◯◯◯◯
                                               ◯◯◯◯
                                               ◯◯◯◯
InformationStateTensor(0).player_action_sequence: ◯◯◯◯
                                                  ◯◯◯◯
                                                  ◯◯◯◯
                                                  ◯◯◯◯
InformationStateTensor(1).turn_based_sim_played_action: ◯◯◯◯
InformationStateTensor(1).turn_based_sim_progress: ◉◯
InformationStateTensor(1).point_totals: ◉◯◯◯◯◯◯◯◯◯◯
                                        ◉◯◯◯◯◯◯◯◯◯◯
InformationStateTensor(1).player_hand: ◉◉◉◉
InformationStateTensor(1).win_sequence: ◯◯
                                        ◯◯
                                        ◯◯
                                        ◯◯
InformationStateTensor(1).tie_sequence: ◯◯◯◯
InformationStateTensor(1).point_card_sequence: ◯◯◯◉
                                               ◯◯◯◯
                                               ◯◯◯◯
                                               ◯◯◯◯
InformationStateTensor(1).player_action_sequence: ◯◯◯◯
                                                  ◯◯◯◯
                                                  ◯◯◯◯
                                                  ◯◯◯◯
ObservationString(0) = "Current player: 0\nCurrent point card: 4 Remaining Point Cards: 123 Points: 0 0  P0 hand: 1 2 3 4  Win sequence:  "
ObservationString(1) = "Current player: 0\nCurrent point card: 4 Remaining Point Cards: 123 Points: 0 0  P1 hand: 1 2 3 4  Win sequence:  "
PublicObservationString() = "Current player: 0\nCurrent point card: 4 Remaining Point Cards: 123 Win sequence:  Points: 0 0  "
PrivateObservationString(0) = "Current point card: 4 Remaining Point Cards: 123 Points: 0 0  P0 hand: 1 2 3 4  Win sequence:  "
PrivateObservationString(1) = "Current point card: 4 Remaining Point Cards: 123 Points: 0 0  P1 hand: 1 2 3 4  Win sequence:  "
ObservationTensor(0).turn_based_sim_played_action: ◯◯◯◯
ObservationTensor(0).turn_based_sim_progress: ◉◯
ObservationTensor(0).current_point_card: ◯◯◯◉
ObservationTensor(0).remaining_point_cards: ◉◉◉◯
ObservationTensor(0).point_totals: ◉◯◯◯◯◯◯◯◯◯◯
                                   ◉◯◯◯◯◯◯◯◯◯◯
ObservationTensor(0).player_hand: ◉◉◉◉
ObservationTensor(0).win_sequence: ◯◯
                                   ◯◯
                                   ◯◯
                                   ◯◯
ObservationTensor(0).tie_sequence: ◯◯◯◯
ObservationTensor(1).turn_based_sim_played_action: ◯◯◯◯
ObservationTensor(1).turn_based_sim_progress: ◉◯
ObservationTensor(1).current_point_card: ◯◯◯◉
ObservationTensor(1).remaining_point_cards: ◉◉◉◯
ObservationTensor(1).point_totals: ◉◯◯◯◯◯◯◯◯◯◯
                                   ◉◯◯◯◯◯◯◯◯◯◯
ObservationTensor(1).player_hand: ◉◉◉◉
ObservationTensor(1).win_sequence: ◯◯
                                   ◯◯
                                   ◯◯
                                   ◯◯
ObservationTensor(1).tie_sequence: ◯◯◯◯
Rewards() = [0.0, 0.0]
Returns() = [0.0, 0.0]
LegalActions() = [0, 1, 2, 3]
StringLegalActions() = ["[P0]Bid: 1", "[P0]Bid: 2", "[P0]Bid: 3", "[P0]Bid: 4"]

# Apply action "[P0]Bid: 3"
action: 2

# State 1
# Partial joint action: 2
# P0 hand: 1 2 3 4  P1 hand: 1 2 3 4  P0 actions:  P1 actions:  Point card sequence: 4  Points: 0 0
IsTerminal() = False
History() = [2]
HistoryString() = "2"
IsChanceNode() = False
IsSimultaneousNode() = False
CurrentPlayer() = 1
InformationStateString(0) = "Current player: 1\nPlayer 0 picked 2\nP0 hand: 1 2 3 4  P0 action sequence:  Point card sequence: 4  Win sequence:  Points: 0 0  Terminal?: 0 "
InformationStateString(1) = "Current player: 1\nP1 hand: 1 2 3 4  P1 action sequence:  Point card sequence: 4  Win sequence:  Points: 0 0  Terminal?: 0 "
InformationStateTensor(0).turn_based_sim_played_action: ◯◯◉◯
InformationStateTensor(0).turn_based_sim_progress: ◯◉
InformationStateTensor(0).point_totals: ◉◯◯◯◯◯◯◯◯◯◯
                                        ◉◯◯◯◯◯◯◯◯◯◯
InformationStateTensor(0).player_hand: ◉◉◉◉
InformationStateTensor(0).win_sequence: ◯◯
                                        ◯◯
                                        ◯◯
                                        ◯◯
InformationStateTensor(0).tie_sequence: ◯◯◯◯
InformationStateTensor(0).point_card_sequence: ◯◯◯◉
                                               ◯◯◯◯
                                               ◯◯◯◯
                                               ◯◯◯◯
InformationStateTensor(0).player_action_sequence: ◯◯◯◯
                                                  ◯◯◯◯
                                                  ◯◯◯◯
                                                  ◯◯◯◯
InformationStateTensor(1).turn_based_sim_played_action: ◯◯◯◯
InformationStateTensor(1).turn_based_sim_progress: ◯◉
InformationStateTensor(1).point_totals: ◉◯◯◯◯◯◯◯◯◯◯
                                        ◉◯◯◯◯◯◯◯◯◯◯
InformationStateTensor(1).player_hand: ◉◉◉◉
InformationStateTensor(1).win_sequence: ◯◯
                                        ◯◯
                                        ◯◯
                                        ◯◯
InformationStateTensor(1).tie_sequence: ◯◯◯◯
InformationStateTensor(1).point_card_sequence: ◯◯◯◉
                                               ◯◯◯◯
                                               ◯◯◯◯
                                               ◯◯◯◯
InformationStateTensor(1).player_action_sequence: ◯◯◯◯
                                                  ◯◯◯◯
                                                  ◯◯◯◯
                                                  ◯◯◯◯
ObservationString(0) = "Current player: 1\nPlayer 0 picked 2\nCurrent point card: 4 Remaining Point Cards: 123 Points: 0 0  P0 hand: 1 2 3 4  Win sequence:  "
ObservationString(1) = "Current player: 1\nCurrent point card: 4 Remaining Point Cards: 123 Points: 0 0  P1 hand: 1 2 3 4  Win sequence:  "
PublicObservationString() = "Current player: 1\nCurrent point card: 4 Remaining Point Cards: 123 Win sequence:  Points: 0 0  "
PrivateObservationString(0) = "Player 0 picked 2\nCurrent point card: 4 Remaining Point Cards: 123 Points: 0 0  P0 hand: 1 2 3 4  Win sequence:  "
PrivateObservationString(1) = "Current point card: 4 Remaining Point Cards: 123 Points: 0 0  P1 hand: 1 2 3 4  Win sequence:  "
ObservationTensor(0).turn_based_sim_played_action: ◯◯◉◯
ObservationTensor(0).turn_based_sim_progress: ◯◉
ObservationTensor(0).current_point_card: ◯◯◯◉
ObservationTensor(0).remaining_point_cards: ◉◉◉◯
ObservationTensor(0).point_totals: ◉◯◯◯◯◯◯◯◯◯◯
                                   ◉◯◯◯◯◯◯◯◯◯◯
ObservationTensor(0).player_hand: ◉◉◉◉
ObservationTensor(0).win_sequence: ◯◯
                                   ◯◯
                                   ◯◯
                                   ◯◯
ObservationTensor(0).tie_sequence: ◯◯◯◯
ObservationTensor(1).turn_based_sim_played_action: ◯◯◯◯
ObservationTensor(1).turn_based_sim_progress: ◯◉
ObservationTensor(1).current_point_card: ◯◯◯◉
ObservationTensor(1).remaining_point_cards: ◉◉◉◯
ObservationTensor(1).point_totals: ◉◯◯◯◯◯◯◯◯◯◯
                                   ◉◯◯◯◯◯◯◯◯◯◯
ObservationTensor(1).player_hand: ◉◉◉◉
ObservationTensor(1).win_sequence: ◯◯
                                   ◯◯
                                   ◯◯
                                   ◯◯
ObservationTensor(1).tie_sequence: ◯◯◯◯
Rewards() = [0.0, 0.0]
Returns() = [0.0, 0.0]
LegalActions() = [0, 1, 2, 3]
StringLegalActions() = ["[P1]Bid: 1", "[P1]Bid: 2", "[P1]Bid: 3", "[P1]Bid: 4"]

# Apply action "[P1]Bid: 4"
action: 3

# State 2
# Partial joint action:
# P0 hand: 1 2 4  P1 hand: 1 2 3  P0 actions: 2  P1 actions: 3  Point card sequence: 4 3  Points: 0 4
IsTerminal() = False
History() = [2, 3]
HistoryString() = "2, 3"
IsChanceNode() = False
IsSimultaneousNode() = False
CurrentPlayer() = 0
InformationStateString(0) = "Current player: 0\nPlayer 0 picked 2\nP0 hand: 1 2 4  P0 action sequence: 2  Point card sequence: 4 3  Win sequence: 1  Points: 0 4  Terminal?: 0 "
InformationStateString(1) = "Current player: 0\nPlayer 1 picked 3\nP1 hand: 1 2 3  P1 action sequence: 3  Point card sequence: 4 3  Win sequence: 1  Points: 0 4  Terminal?: 0 "
InformationStateTensor(0).turn_based_sim_played_action: ◯◯◉◯
InformationStateTensor(0).turn_based_sim_progress: ◉◯
InformationStateTensor(0).point_totals: ◉◯◯◯◯◯◯◯◯◯◯
                                        ◯◯◯◯◉◯◯◯◯◯◯
InformationStateTensor(0).player_hand: ◉◉◯◉
InformationStateTensor(0).win_sequence: ◯◉
                                        ◯◯
                                        ◯◯
                                        ◯◯
InformationStateTensor(0).tie_sequence: ◯◯◯◯
InformationStateTensor(0).point_card_sequence: ◯◯◯◉
                                               ◯◯◉◯
                                               ◯◯◯◯
                                               ◯◯◯◯
InformationStateTensor(0).player_action_sequence: ◯◯◉◯
                                                  ◯◯◯◯
                                                  ◯◯◯◯
                                                  ◯◯◯◯
InformationStateTensor(1).turn_based_sim_played_action: ◯◯◯◉
InformationStateTensor(1).turn_based_sim_progress: ◉◯
InformationStateTensor(1).point_totals: ◯◯◯◯◉◯◯◯◯◯◯
                                        ◉◯◯◯◯◯◯◯◯◯◯
InformationStateTensor(1).player_hand: ◉◉◉◯
InformationStateTensor(1).win_sequence: ◯◉
                                        ◯◯
                                        ◯◯
                                        ◯◯
InformationStateTensor(1).tie_sequence: ◯◯◯◯
InformationStateTensor(1).point_card_sequence: ◯◯◯◉
                                               ◯◯◉◯
                                               ◯◯◯◯
                                               ◯◯◯◯
InformationStateTensor(1).player_action_sequence: ◯◯◯◉
                                                  ◯◯◯◯
                                                  ◯◯◯◯
                                                  ◯◯◯◯
ObservationString(0) = "Current player: 0\nPlayer 0 picked 2\nCurrent point card: 3 Remaining Point Cards: 12 Points: 0 4  P0 hand: 1 2 4  Win sequence: 1  "
ObservationString(1) = "Current player: 0\nPlayer 1 picked 3\nCurrent point card: 3 Remaining Point Cards: 12 Points: 0 4  P1 hand: 1 2 3  Win sequence: 1  "
PublicObservationString() = "Current player: 0\nCurrent point card: 3 Remaining Point Cards: 12 Win sequence: 1  Points: 0 4  "
PrivateObservationString(0) = "Player 0 picked 2\nCurrent point card: 3 Remaining Point Cards: 12 Points: 0 4  P0 hand: 1 2 4  Win sequence: 1  "
PrivateObservationString(1) = "Player 1 picked 3\nCurrent point card: 3 Remaining Point Cards: 12 Points: 0 4  P1 hand: 1 2 3  Win sequence: 1  "
ObservationTensor(0).turn_based_sim_played_action: ◯◯◉◯
ObservationTensor(0).turn_based_sim_progress: ◉◯
ObservationTensor(0).current_point_card: ◯◯◉◯
ObservationTensor(0).remaining_point_cards: ◉◉◯◯
ObservationTensor(0).point_totals: ◉◯◯◯◯◯◯◯◯◯◯
                                   ◯◯◯◯◉◯◯◯◯◯◯
ObservationTensor(0).player_hand: ◉◉◯◉
ObservationTensor(0).win_sequence: ◯◉
                                   ◯◯
                                   ◯◯
                                   ◯◯
ObservationTensor(0).tie_sequence: ◯◯◯◯
ObservationTensor(1).turn_based_sim_played_action: ◯◯◯◉
ObservationTensor(1).turn_based_sim_progress: ◉◯
ObservationTensor(1).current_point_card: ◯◯◉◯
ObservationTensor(1).remaining_point_cards: ◉◉◯◯
ObservationTensor(1).point_totals: ◯◯◯◯◉◯◯◯◯◯◯
                                   ◉◯◯◯◯◯◯◯◯◯◯
ObservationTensor(1).player_hand: ◉◉◉◯
ObservationTensor(1).win_sequence: ◯◉
                                   ◯◯
                                   ◯◯
                                   ◯◯
ObservationTensor(1).tie_sequence: ◯◯◯◯
Rewards() = [0.0, 0.0]
Returns() = [0.0, 0.0]
LegalActions() = [0, 1, 3]
StringLegalActions() = ["[P0]Bid: 1", "[P0]Bid: 2", "[P0]Bid: 4"]

# Apply action "[P0]Bid: 2"
action: 1

# State 3
# Partial joint action: 1
# P0 hand: 1 2 4  P1 hand: 1 2 3  P0 actions: 2  P1 actions: 3  Point card sequence: 4 3  Points: 0 4
IsTerminal() = False
History() = [2, 3, 1]
HistoryString() = "2, 3, 1"
IsChanceNode() = False
IsSimultaneousNode() = False
CurrentPlayer() = 1
InformationStateString(0) = "Current player: 1\nPlayer 0 picked 1\nP0 hand: 1 2 4  P0 action sequence: 2  Point card sequence: 4 3  Win sequence: 1  Points: 0 4  Terminal?: 0 "
InformationStateString(1) = "Current player: 1\nPlayer 1 picked 3\nP1 hand: 1 2 3  P1 action sequence: 3  Point card sequence: 4 3  Win sequence: 1  Points: 0 4  Terminal?: 0 "
InformationStateTensor(0).turn_based_sim_played_action: ◯◉◯◯
InformationStateTensor(0).turn_based_sim_progress: ◯◉
InformationStateTensor(0).point_totals: ◉◯◯◯◯◯◯◯◯◯◯
                                        ◯◯◯◯◉◯◯◯◯◯◯
InformationStateTensor(0).player_hand: ◉◉◯◉
InformationStateTensor(0).win_sequence: ◯◉
                                        ◯◯
                                        ◯◯
                                        ◯◯
InformationStateTensor(0).tie_sequence: ◯◯◯◯
InformationStateTensor(0).point_card_sequence: ◯◯◯◉
                                               ◯◯◉◯
                                               ◯◯◯◯
                                               ◯◯◯◯
InformationStateTensor(0).player_action_sequence: ◯◯◉◯
                                                  ◯◯◯◯
                                                  ◯◯◯◯
                                                  ◯◯◯◯
InformationStateTensor(1).turn_based_sim_played_action: ◯◯◯◉
InformationStateTensor(1).turn_based_sim_progress: ◯◉
InformationStateTensor(1).point_totals: ◯◯◯◯◉◯◯◯◯◯◯
                                        ◉◯◯◯◯◯◯◯◯◯◯
InformationStateTensor(1).player_hand: ◉◉◉◯
InformationStateTensor(1).win_sequence: ◯◉
                                        ◯◯
                                        ◯◯
                                        ◯◯
InformationStateTensor(1).tie_sequence: ◯◯◯◯
InformationStateTensor(1).point_card_sequence: ◯◯◯◉
                                               ◯◯◉◯
                                               ◯◯◯◯
                                               ◯◯◯◯
InformationStateTensor(1).player_action_sequence: ◯◯◯◉
                                                  ◯◯◯◯
                                                  ◯◯◯◯
                                                  ◯◯◯◯
ObservationString(0) = "Current player: 1\nPlayer 0 picked 1\nCurrent point card: 3 Remaining Point Cards: 12 Points: 0 4  P0 hand: 1 2 4  Win sequence: 1  "
ObservationString(1) = "Current player: 1\nPlayer 1 picked 3\nCurrent point card: 3 Remaining Point Cards: 12 Points: 0 4  P1 hand: 1 2 3  Win sequence: 1  "
PublicObservationString() = "Current player: 1\nCurrent point card: 3 Remaining Point Cards: 12 Win sequence: 1  Points: 0 4  "
PrivateObservationString(0) = "Player 0 picked 1\nCurrent point card: 3 Remaining Point Cards: 12 Points: 0 4  P0 hand: 1 2 4  Win sequence: 1  "
PrivateObservationString(1) = "Player 1 picked 3\nCurrent point card: 3 Remaining Point Cards: 12 Points: 0 4  P1 hand: 1 2 3  Win sequence: 1  "
ObservationTensor(0).turn_based_sim_played_action: ◯◉◯◯
ObservationTensor(0).turn_based_sim_progress: ◯◉
ObservationTensor(0).current_point_card: ◯◯◉◯
ObservationTensor(0).remaining_point_cards: ◉◉◯◯
ObservationTensor(0).point_totals: ◉◯◯◯◯◯◯◯◯◯◯
                                   ◯◯◯◯◉◯◯◯◯◯◯
ObservationTensor(0).player_hand: ◉◉◯◉
ObservationTensor(0).win_sequence: ◯◉
                                   ◯◯
                                   ◯◯
                                   ◯◯
ObservationTensor(0).tie_sequence: ◯◯◯◯
ObservationTensor(1).turn_based_sim_played_action: ◯◯◯◉
ObservationTensor(1).turn_based_sim_progress: ◯◉
ObservationTensor(1).current_point_card: ◯◯◉◯
ObservationTensor(1).remaining_point_cards: ◉◉◯◯
ObservationTensor(1).point_totals: ◯◯◯◯◉◯◯◯◯◯◯
                                   ◉◯◯◯◯◯◯◯◯◯◯
ObservationTensor(1).player_hand: ◉◉◉◯
ObservationTensor(1).win_sequence: ◯◉
                                   ◯◯
                                   ◯◯
                                   ◯◯
ObservationTensor(1).tie_sequence: ◯◯◯◯
Rewards() = [0.0, 0.0]
Returns() = [0.0, 0.0]
LegalActions() = [0, 1, 2]
StringLegalActions() = ["[P1]Bid: 1", "[P1]Bid: 2", "[P1]Bid: 3"]

# Apply action "[P1]Bid: 1"
action: 0

# State 4
# Partial joint action:
# P0 hand: 1 4  P1 hand: 2 3  P0 actions: 2 1  P1 actions: 3 0  Point card sequence: 4 3 2  Points: 3 4
IsTerminal() = False
History() = [2, 3, 1, 0]
HistoryString() = "2, 3, 1, 0"
IsChanceNode() = False
IsSimultaneousNode() = False
CurrentPlayer() = 0
InformationStateString(0) = "Current player: 0\nPlayer 0 picked 1\nP0 hand: 1 4  P0 action sequence: 2 1  Point card sequence: 4 3 2  Win sequence: 1 0  Points: 3 4  Terminal?: 0 "
InformationStateString(1) = "Current player: 0\nPlayer 1 picked 0\nP1 hand: 2 3  P1 action sequence: 3 0  Point card sequence: 4 3 2  Win sequence: 1 0  Points: 3 4  Terminal?: 0 "
InformationStateTensor(0).turn_based_sim_played_action: ◯◉◯◯
InformationStateTensor(0).turn_based_sim_progress: ◉◯
InformationStateTensor(0).point_totals: ◯◯◯◉◯◯◯◯◯◯◯
                                        ◯◯◯◯◉◯◯◯◯◯◯
InformationStateTensor(0).player_hand: ◉◯◯◉
InformationStateTensor(0).win_sequence: ◯◉
                                        ◉◯
                                        ◯◯
                                        ◯◯
InformationStateTensor(0).tie_sequence: ◯◯◯◯
InformationStateTensor(0).point_card_sequence: ◯◯◯◉
                                               ◯◯◉◯
                                               ◯◉◯◯
                                               ◯◯◯◯
InformationStateTensor(0).player_action_sequence: ◯◯◉◯
                                                  ◯◉◯◯
                                                  ◯◯◯◯
                                                  ◯◯◯◯
InformationStateTensor(1).turn_based_sim_played_action: ◉◯◯◯
InformationStateTensor(1).turn_based_sim_progress: ◉◯
InformationStateTensor(1).point_totals: ◯◯◯◯◉◯◯◯◯◯◯
                                        ◯◯◯◉◯◯◯◯◯◯◯
InformationStateTensor(1).player_hand: ◯◉◉◯
InformationStateTensor(1).win_sequence: ◯◉
                                        ◉◯
                                        ◯◯
                                        ◯◯
InformationStateTensor(1).tie_sequence: ◯◯◯◯
InformationStateTensor(1).point_card_sequence: ◯◯◯◉
                                               ◯◯◉◯
                                               ◯◉◯◯
                                               ◯◯◯◯
InformationStateTensor(1).player_action_sequence: ◯◯◯◉
                                                  ◉◯◯◯
                                                  ◯◯◯◯
                                                  ◯◯◯◯
ObservationString(0) = "Current player: 0\nPlayer 0 picked 1\nCurrent point card: 2 Remaining Point Cards: 1 Points: 3 4  P0 hand: 1 4  Win sequence: 1 0  "
ObservationString(1) = "Current player: 0\nPlayer 1 picked 0\nCurrent point card: 2 Remaining Point Cards: 1 Points: 3 4  P1 hand: 2 3  Win sequence: 1 0  "
PublicObservationString() = "Current player: 0\nCurrent point card: 2 Remaining Point Cards: 1 Win sequence: 1 0  Points: 3 4  "
PrivateObservationString(0) = "Player 0 picked 1\nCurrent point card: 2 Remaining Point Cards: 1 Points: 3 4  P0 hand: 1 4  Win sequence: 1 0  "
PrivateObservationString(1) = "Player 1 picked 0\nCurrent point card: 2 Remaining Point Cards: 1 Points: 3 4  P1 hand: 2 3  Win sequence: 1 0  "
ObservationTensor(0).turn_based_sim_played_action: ◯◉◯◯
ObservationTensor(0).turn_based_sim_progress: ◉◯
ObservationTensor(0).current_point_card: ◯◉◯◯
ObservationTensor(0).remaining_point_cards: ◉◯◯◯
ObservationTensor(0).point_totals: ◯◯◯◉◯◯◯◯◯◯◯
                                   ◯◯◯◯◉◯◯◯◯◯◯
ObservationTensor(0).player_hand: ◉◯◯◉
ObservationTensor(0).win_sequence: ◯◉
                                   ◉◯
                                   ◯◯
                                   ◯◯
ObservationTensor(0).tie_sequence: ◯◯◯◯
ObservationTensor(1).turn_based_sim_played_action: ◉◯◯◯
ObservationTensor(1).turn_based_sim_progress: ◉◯
ObservationTensor(1).current_point_card: ◯◉◯◯
ObservationTensor(1).remaining_point_cards: ◉◯◯◯
ObservationTensor(1).point_totals: ◯◯◯◯◉◯◯◯◯◯◯
                                   ◯◯◯◉◯◯◯◯◯◯◯
ObservationTensor(1).player_hand: ◯◉◉◯
ObservationTensor(1).win_sequence: ◯◉
                                   ◉◯
                                   ◯◯
                                   ◯◯
ObservationTensor(1).tie_sequence: ◯◯◯◯
Rewards() = [0.0, 0.0]
Returns() = [0.0, 0.0]
LegalActions() = [0, 3]
StringLegalActions() = ["[P0]Bid: 1", "[P0]Bid: 4"]

# Apply action "[P0]Bid: 4"
action: 3

# State 5
# Partial joint action: 3
# P0 hand: 1 4  P1 hand: 2 3  P0 actions: 2 1  P1 actions: 3 0  Point card sequence: 4 3 2  Points: 3 4
IsTerminal() = False
History() = [2, 3, 1, 0, 3]
HistoryString() = "2, 3, 1, 0, 3"
IsChanceNode() = False
IsSimultaneousNode() = False
CurrentPlayer() = 1
InformationStateString(0) = "Current player: 1\nPlayer 0 picked 3\nP0 hand: 1 4  P0 action sequence: 2 1  Point card sequence: 4 3 2  Win sequence: 1 0  Points: 3 4  Terminal?: 0 "
InformationStateString(1) = "Current player: 1\nPlayer 1 picked 0\nP1 hand: 2 3  P1 action sequence: 3 0  Point card sequence: 4 3 2  Win sequence: 1 0  Points: 3 4  Terminal?: 0 "
InformationStateTensor(0).turn_based_sim_played_action: ◯◯◯◉
InformationStateTensor(0).turn_based_sim_progress: ◯◉
InformationStateTensor(0).point_totals: ◯◯◯◉◯◯◯◯◯◯◯
                                        ◯◯◯◯◉◯◯◯◯◯◯
InformationStateTensor(0).player_hand: ◉◯◯◉
InformationStateTensor(0).win_sequence: ◯◉
                                        ◉◯
                                        ◯◯
                                        ◯◯
InformationStateTensor(0).tie_sequence: ◯◯◯◯
InformationStateTensor(0).point_card_sequence: ◯◯◯◉
                                               ◯◯◉◯
                                               ◯◉◯◯
                                               ◯◯◯◯
InformationStateTensor(0).player_action_sequence: ◯◯◉◯
                                                  ◯◉◯◯
                                                  ◯◯◯◯
                                                  ◯◯◯◯
InformationStateTensor(1).turn_based_sim_played_action: ◉◯◯◯
InformationStateTensor(1).turn_based_sim_progress: ◯◉
InformationStateTensor(1).point_totals: ◯◯◯◯◉◯◯◯◯◯◯
                                        ◯◯◯◉◯◯◯◯◯◯◯
InformationStateTensor(1).player_hand: ◯◉◉◯
InformationStateTensor(1).win_sequence: ◯◉
                                        ◉◯
                                        ◯◯
                                        ◯◯
InformationStateTensor(1).tie_sequence: ◯◯◯◯
InformationStateTensor(1).point_card_sequence: ◯◯◯◉
                                               ◯◯◉◯
                                               ◯◉◯◯
                                               ◯◯◯◯
InformationStateTensor(1).player_action_sequence: ◯◯◯◉
                                                  ◉◯◯◯
                                                  ◯◯◯◯
                                                  ◯◯◯◯
ObservationString(0) = "Current player: 1\nPlayer 0 picked 3\nCurrent point card: 2 Remaining Point Cards: 1 Points: 3 4  P0 hand: 1 4  Win sequence: 1 0  "
ObservationString(1) = "Current player: 1\nPlayer 1 picked 0\nCurrent point card: 2 Remaining Point Cards: 1 Points: 3 4  P1 hand: 2 3  Win sequence: 1 0  "
PublicObservationString() = "Current player: 1\nCurrent point card: 2 Remaining Point Cards: 1 Win sequence: 1 0  Points: 3 4  "
PrivateObservationString(0) = "Player 0 picked 3\nCurrent point card: 2 Remaining Point Cards: 1 Points: 3 4  P0 hand: 1 4  Win sequence: 1 0  "
PrivateObservationString(1) = "Player 1 picked 0\nCurrent point card: 2 Remaining Point Cards: 1 Points: 3 4  P1 hand: 2 3  Win sequence: 1 0  "
ObservationTensor(0).turn_based_sim_played_action: ◯◯◯◉
ObservationTensor(0).turn_based_sim_progress: ◯◉
ObservationTensor(0).current_point_card: ◯◉◯◯
ObservationTensor(0).remaining_point_cards: ◉◯◯◯
ObservationTensor(0).point_totals: ◯◯◯◉◯◯◯◯◯◯◯
                                   ◯◯◯◯◉◯◯◯◯◯◯
ObservationTensor(0).player_hand: ◉◯◯◉
ObservationTensor(0).win_sequence: ◯◉
                                   ◉◯
                                   ◯◯
                                   ◯◯
ObservationTensor(0).tie_sequence: ◯◯◯◯
ObservationTensor(1).turn_based_sim_played_action: ◉◯◯◯
ObservationTensor(1).turn_based_sim_progress: ◯◉
ObservationTensor(1).current_point_card: ◯◉◯◯
ObservationTensor(1).remaining_point_cards: ◉◯◯◯
ObservationTensor(1).point_totals: ◯◯◯◯◉◯◯◯◯◯◯
                                   ◯◯◯◉◯◯◯◯◯◯◯
ObservationTensor(1).player_hand: ◯◉◉◯
ObservationTensor(1).win_sequence: ◯◉
                                   ◉◯
                                   ◯◯
                                   ◯◯
ObservationTensor(1).tie_sequence: ◯◯◯◯
Rewards() = [0.0, 0.0]
Returns() = [0.0, 0.0]
LegalActions() = [1, 2]
StringLegalActions() = ["[P1]Bid: 2", "[P1]Bid: 3"]

# Apply action "[P1]Bid: 2"
action: 1

# State 6
# P0 hand:  P1 hand:  P0 actions: 2 1 3 0  P1 actions: 3 0 1 2  Point card sequence: 4 3 2 1  Points: 5 5
IsTerminal() = True
History() = [2, 3, 1, 0, 3, 1]
HistoryString() = "2, 3, 1, 0, 3, 1"
IsChanceNode() = False
IsSimultaneousNode() = False
CurrentPlayer() = -4
InformationStateString(0) = "Current player: -4\nPlayer 0 picked 3\nP0 hand:  P0 action sequence: 2 1 3 0  Point card sequence: 4 3 2 1  Win sequence: 1 0 0 1  Points: 5 5  Terminal?: 1 "
InformationStateString(1) = "Current player: -4\nPlayer 1 picked 1\nP1 hand:  P1 action sequence: 3 0 1 2  Point card sequence: 4 3 2 1  Win sequence: 1 0 0 1  Points: 5 5  Terminal?: 1 "
InformationStateTensor(0).turn_based_sim_played_action: ◯◯◯◉
InformationStateTensor(0).turn_based_sim_progress: ◯◯
InformationStateTensor(0).point_totals: ◯◯◯◯◯◉◯◯◯◯◯
                                        ◯◯◯◯◯◉◯◯◯◯◯
InformationStateTensor(0).player_hand: ◯◯◯◯
InformationStateTensor(0).win_sequence: ◯◉
                                        ◉◯
                                        ◉◯
                                        ◯◉
InformationStateTensor(0).tie_sequence: ◯◯◯◯
InformationStateTensor(0).point_card_sequence: ◯◯◯◉
                                               ◯◯◉◯
                                               ◯◉◯◯
                                               ◉◯◯◯
InformationStateTensor(0).player_action_sequence: ◯◯◉◯
                                                  ◯◉◯◯
                                                  ◯◯◯◉
                                                  ◉◯◯◯
InformationStateTensor(1).turn_based_sim_played_action: ◯◉◯◯
InformationStateTensor(1).turn_based_sim_progress: ◯◯
InformationStateTensor(1).point_totals: ◯◯◯◯◯◉◯◯◯◯◯
                                        ◯◯◯◯◯◉◯◯◯◯◯
InformationStateTensor(1).player_hand: ◯◯◯◯
InformationStateTensor(1).win_sequence: ◯◉
                                        ◉◯
                                        ◉◯
                                        ◯◉
InformationStateTensor(1).tie_sequence: ◯◯◯◯
InformationStateTensor(1).point_card_sequence: ◯◯◯◉
                                               ◯◯◉◯
                                               ◯◉◯◯
                                               ◉◯◯◯
InformationStateTensor(1).player_action_sequence: ◯◯◯◉
                                                  ◉◯◯◯
                                                  ◯◉◯◯
                                                  ◯◯◉◯
ObservationString(0) = "Current player: -4\nPlayer 0 picked 3\nCurrent point card: 1 Remaining Point Cards:  Points: 5 5  P0 hand:  Win sequence: 1 0 0 1  "
ObservationString(1) = "Current player: -4\nPlayer 1 picked 1\nCurrent point card: 1 Remaining Point Cards:  Points: 5 5  P1 hand:  Win sequence: 1 0 0 1  "
PublicObservationString() = "Current player: -4\nCurrent point card: 1 Remaining Point Cards:  Win sequence: 1 0 0 1  Points: 5 5  "
PrivateObservationString(0) = "Player 0 picked 3\nCurrent point card: 1 Remaining Point Cards:  Points: 5 5  P0 hand:  Win sequence: 1 0 0 1  "
PrivateObservationString(1) = "Player 1 picked 1\nCurrent point card: 1 Remaining Point Cards:  Points: 5 5  P1 hand:  Win sequence: 1 0 0 1  "
ObservationTensor(0).turn_based_sim_played_action: ◯◯◯◉
ObservationTensor(0).turn_based_sim_progress: ◯◯
ObservationTensor(0).current_point_card: ◉◯◯◯
ObservationTensor(0).remaining_point_cards: ◯◯◯◯
ObservationTensor(0).point_totals: ◯◯◯◯◯◉◯◯◯◯◯
                                   ◯◯◯◯◯◉◯◯◯◯◯
ObservationTensor(0).player_hand: ◯◯◯◯
ObservationTensor(0).win_sequence: ◯◉
                                   ◉◯
                                   ◉◯
                                   ◯◉
ObservationTensor(0).tie_sequence: ◯◯◯◯
ObservationTensor(1).turn_based_sim_played_action: ◯◉◯◯
ObservationTensor(1).turn_based_sim_progress: ◯◯
ObservationTensor(1).current_point_card: ◉◯◯◯
ObservationTensor(1).remaining_point_cards: ◯◯◯◯
ObservationTensor(1).point_totals: ◯◯◯◯◯◉◯◯◯◯◯
                                   ◯◯◯◯◯◉◯◯◯◯◯
ObservationTensor(1).player_hand: ◯◯◯◯
ObservationTensor(1).win_sequence: ◯◉
                                   ◉◯
                                   ◉◯
                                   ◯◉
ObservationTensor(1).tie_sequence: ◯◯◯◯
Rewards() = [0.0, 0.0]
Returns() = [0.0, 0.0]
